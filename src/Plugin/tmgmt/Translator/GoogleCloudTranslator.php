<?php

namespace Drupal\tmgmt_google_cloud\Plugin\tmgmt\Translator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\tmgmt\ContinuousTranslatorInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\TranslatorPluginBase;
use Drupal\tmgmt\Translator\AvailableResult;
use Drupal\tmgmt\Translator\TranslatableResult;

use Google\Cloud\Translate\V2\TranslateClient;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Google cloud translator plugin.
 *
 * @TranslatorPlugin(
 *   id          = "google_cloud_translator",
 *   label       = @Translation("Google Cloud Translator - Basic"),
 *   description = @Translation("Google Cloud Translator service."),
 *   ui          = "Drupal\tmgmt_google_cloud\GoogleCloudUI",
 *   logo        = "icons/google.svg",
 * )
 */
class GoogleCloudTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface, ContinuousTranslatorInterface {
  /**
   * @var \Google\Cloud\Translate\V2\TranslateClient[]
   */
  protected static $clients = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::checkAvailable().
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ($translator->getSetting('api_key')) {
      return AvailableResult::yes();
    }

    return AvailableResult::no(t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl()->toString()
    ]));
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(JobInterface $job) {
    $this->requestJobItemsTranslation($job->getItems());
    if (!$job->isRejected()) {
      $job->submitted('The translation job has been submitted.');
    }
  }

  /**
   * Requests the translation of a JobItem.
   *
   * @param JobItemInterface[] $job_items
   *   The JobItem we want to translate.
   */
  public function requestJobItemsTranslation(array $job_items) {
    /** @var \Drupal\tmgmt\Entity\Job $job */
    $job = reset($job_items)->getJob();

    /** @var \Drupal\tmgmt\Entity\JobItem $job_item */
    foreach ($job_items as $job_item) {
      if ($job->isContinuous()) {
        $job_item->active();
      }

      // Pull the source data array through the job and flatten it.
      $data = \Drupal::service('tmgmt.data')->filterTranslatable($job_item->getData());
      $translation = array();
      foreach ($data as $key => $value) {
        try {
          $response = $this->getTranslateClient( $job->getTranslator()->getSetting('api_key') )->translate($value['#text'], [
            'source' => $job->getRemoteSourceLanguage(),
            'target' => $job->getRemoteTargetLanguage(),
            'format' => 'html',
          ]);

          if ( isset($response['text']) ){
            $translation[$key]['#text'] = $response['text'];
          }

          // Save the translated data through the job.
          $job_item->addTranslatedData(\Drupal::service('tmgmt.data')->unflatten($translation));

        } catch (Exception $e) {
          $message = @json_decode($e->getMessage(), true);
          if ( $message !== false && isset($message['error']['code']) && isset($message['error']['message']) ){
            $message = '['.$message['error']['code'].'] ' . $message['error']['message'];
          } else {
            $message = $e->getMessage();
          }
          $job->rejected('Rejected by Google Cloud Translator: @error', array('@error' => $message), 'error');
        }
      }
    }
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getSupportedRemoteLanguages().
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    static $languages = NULL;

    if ( is_null($languages) ){
      // Prevent access if the translator isn't configured yet.
      if ( !$translator->getSetting('api_key') ) {
        return array();
      }

      try {
        $languages = $this->getTranslateClient( $translator->getSetting('api_key') )->languages();
        $languages = array_combine($languages, $languages);
      } catch (\Exception $e) {
        \Drupal::messenger()->addMessage($e->getMessage(), 'error');
        return array();
      }
    }
    return $languages;
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getSupportedTargetLanguages().
   */
  public function getSupportedTargetLanguages(TranslatorInterface $translator, $source_language) {
    $languages = $this->getSupportedRemoteLanguages($translator);
    if (array_key_exists($source_language, $languages)) {
      unset($languages[$source_language]);
      return $languages;
    }
    return array();
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::hasCheckoutSettings().
   */
  public function hasCheckoutSettings(JobInterface $job) {
    return FALSE;
  }

  /**
   * Create TranslateClient
   *
   * @param string $apiKey
   * @return TranslateClient
   */
  protected function getTranslateClient(string $apiKey){
    if ( !isset(self::$clients[$apiKey]) ){
      $options = array();
      if ( !empty($apiKey) ){
        $options['key'] = $apiKey;
      }
      self::$clients[$apiKey] = new TranslateClient($options);
    }

    return self::$clients[$apiKey];
  }
}
